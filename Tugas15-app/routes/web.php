<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/table', function () {
    return view('partial.table');
});

Route::get('/data-tables', function () {
    return view('partial.data-tables');
});

//Create
//menampilkan form untuk membuat data pemain film baru
Route::get('/cast/create', [CastController::class,'create']);
//menyimpan data baru ke tabel Cast
Route::post('/cast', [CastController::class,'store']);

//Read
//menampilkan list data para pemain film
Route::get('/cast', [CastController::class,'index']);
//menampilkan detail data pemain film dengan id tertentu
Route::get('/cast/{cast_id}', [CastController::class,'show']);

//Update
//menampilkan form untuk edit pemain film dengan id tertentu
Route::get('/cast/{cast_id}/edit', [CastController::class,'edit']);
//menyimpan perubahan data pemain film (update) untuk id tertentu
Route::put('/cast/{cast_id}', [CastController::class,'update']);

//Delete
//menghapus data pemain film dengan id tertentu
Route::delete('/cast/{cast_id}', [CastController::class,'destroy']);


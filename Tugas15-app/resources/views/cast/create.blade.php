@extends('layout.master')

@section('title')
    Halaman Create Cast
@endsection

@section('sub-title')
    Create Cast
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label for="exampleFormControlInput1">Nama</label>
      <input type="text" name="nama" class="form-control" id="exampleFormControlInput1">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="exampleFormControlInput2">Umur</label>
        <input type="number" name="umur" class="form-control" id="exampleFormControlInput2">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="exampleFormControlTextarea1">Bio</label>
      <textarea class="form-control" name="bio" id="exampleFormControlTextarea1" rows="10"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection

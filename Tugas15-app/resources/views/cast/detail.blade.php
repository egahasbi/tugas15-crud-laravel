@extends('layout.master')

@section('title')
    Halaman Detail Cast
@endsection

@section('sub-title')
    Detail Cast
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<p>Umur : {{$cast->umur}}</p>
<p>Bio : <br>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-primary btn-sm">Kembali</a>


@endsection
